-- forward reference
function Vec2(x,y) end

local vec2_meta = {
   __add = function(a, b)
      return Vec2(a.x + b.x, a.y + b.y)
   end,

   __sub = function(a, b)
      return Vec2(a.x - b.x, a.y - b.y)
   end,

   -- can take arguments, not taking any in
   -- this example
   __call = function(self)
      print("(" .. self.x .. ", " .. self.y .. ")")
   end,

   __tostring = function(self)
      return "(" .. self.x .. ", " .. self.y .. ")"
   end
}

function Vec2(x, y) 
   local v = {
      x = x or 0,
      y = y or 0
   }

   setmetatable(v, vec2_meta)
   return v
end


