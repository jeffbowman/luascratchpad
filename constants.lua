function const(tab)
   local meta_tab = {
      -- overrides what happens when the table is indexed
      __index = function(self, key)
	 -- closes over tab variable, using self causes an error
	 if tab[key] == nil then
	    error("attemp to access non-existant key: " ..  key)
	 else
	    return tab[key]
	 end
      end,

      -- overrides what happens when an assignment is attempted
      -- regardless if the index exists or not
      __newindex = function(self, key, value)
	 error("invalid operation: attempt to assign a value to a key [" .. key .. ", " .. value)
      end,

      __metatable = false
   }

   return setmetatable({}, meta_tab)
end

local Constant = const{GRAVITY=200, SPEED=120}
local Variable = { GRAVITY=100, SPEED=90 }

-- creates a new index with the misspelled key
Variable.GRAVITYY = 10
-- prints the correctly spelled key, the above line
-- "appears" to have no effect since the key
-- spelling is different
print(Variable.GRAVITY)

-- works as expected
print(Constant.GRAVITY)

-- __index error
-- local x = Constant.GRAVITYY

-- __newindex error
-- Constant.SPEED = 100

require("vector")

local a = Vec2(10, 1)
local b = Vec2(20, 2)
local c = a + b

a()
print(c)
print(b - a)
